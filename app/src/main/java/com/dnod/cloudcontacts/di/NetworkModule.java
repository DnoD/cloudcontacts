package com.dnod.cloudcontacts.di;

import android.content.Context;

import com.dnod.cloudcontacts.R;
import com.dnod.cloudcontacts.data.source.ContactsDataSource;
import com.dnod.cloudcontacts.data.source.remote.RemoteContactsDataSource;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    public static Gson provideGson() {
        return new GsonBuilder()
                .create();
    }

    @Provides
    @Singleton
    @Endpoint
    public static String provideEndpoint(Context context) {
        return context.getString(R.string.api_endpoint);
    }

    @Provides
    @Singleton
    public static ContactsDataSource provideContactsDataSource(RemoteContactsDataSource dataSource) {
        return dataSource;
    }
}
