package com.dnod.cloudcontacts.di;

import com.dnod.cloudcontacts.ui.main.MainActivity;
import com.dnod.cloudcontacts.ui.main.MainActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity mainActivity();
}
