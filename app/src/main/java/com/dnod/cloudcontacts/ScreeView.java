package com.dnod.cloudcontacts;

import android.support.annotation.StringRes;

public interface ScreeView<T extends ScreenPresenter> {

    void showMessage(String message);

    void showMessage(@StringRes int message);
}