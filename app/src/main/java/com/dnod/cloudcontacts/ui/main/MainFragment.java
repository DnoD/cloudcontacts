package com.dnod.cloudcontacts.ui.main;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.dnod.cloudcontacts.R;
import com.dnod.cloudcontacts.data.Contact;
import com.dnod.cloudcontacts.data.Group;
import com.dnod.cloudcontacts.databinding.FragmentMainBinding;
import com.dnod.cloudcontacts.ui.Conductor;
import com.dnod.cloudcontacts.ui.base.BaseFragment;
import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

public class MainFragment extends BaseFragment<MainContract.Presenter> implements MainContract.View {

    private static final String SAVED_STATE_EXPANDABLE_ITEM_MANAGER = "RecyclerViewExpandableItemManager";

    public static BaseFragment createInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    Conductor<BaseFragment> conductor;
    @Inject
    MainContract.Presenter presenter;

    private FragmentMainBinding bindingObject;
    private ContactsAdapter contactsAdapter;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewExpandableItemManager recyclerViewExpandableItemManager;
    private List<Group> allData = new ArrayList<>();

    @Inject
    public MainFragment() {
    }

    @NonNull
    @Override
    protected View bindView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {
        bindingObject = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
        setupMenu();
        presenter.loadContacts();
        return bindingObject.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //noinspection ConstantConditions

        final Parcelable eimSavedState = (savedInstanceState != null) ? savedInstanceState.getParcelable(SAVED_STATE_EXPANDABLE_ITEM_MANAGER) : null;
        recyclerViewExpandableItemManager = new RecyclerViewExpandableItemManager(eimSavedState);

        //adapter
        contactsAdapter = new ContactsAdapter(getContext());

        mWrappedAdapter = recyclerViewExpandableItemManager.createWrappedAdapter(contactsAdapter);       // wrap for expanding

        final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();

        // Change animations are enabled by default since support-v7-recyclerview v22.
        // Need to disable them when using animation indicator.
        animator.setSupportsChangeAnimations(false);

        bindingObject.contacts.setLayoutManager(new LinearLayoutManager(getContext()));
        bindingObject.contacts.setAdapter(mWrappedAdapter);  // requires *wrapped* adapter
        bindingObject.contacts.setItemAnimator(animator);
        bindingObject.contacts.setHasFixedSize(false);

        recyclerViewExpandableItemManager.attachRecyclerView(bindingObject.contacts);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        // save current state to support screen rotation, etc...
        if (recyclerViewExpandableItemManager != null) {
            outState.putParcelable(
                    SAVED_STATE_EXPANDABLE_ITEM_MANAGER,
                    recyclerViewExpandableItemManager.getSavedState());
        }
    }

    @Override
    public void onDestroyView() {
        if (recyclerViewExpandableItemManager != null) {
            recyclerViewExpandableItemManager.release();
            recyclerViewExpandableItemManager = null;
        }

        if (bindingObject.contacts != null) {
            bindingObject.contacts.setItemAnimator(null);
            bindingObject.contacts.setAdapter(null);
        }

        if (mWrappedAdapter != null) {
            WrapperAdapterUtils.releaseAll(mWrappedAdapter);
            mWrappedAdapter = null;
        }

        super.onDestroyView();
    }

    @Override
    public void showContacts(List<Group> data) {
        contactsAdapter.swapData(data);
        allData.clear();
        allData.addAll(data);
        recyclerViewExpandableItemManager.expandAll();
        if (data.isEmpty()) {
            bindingObject.stateMessage.setText(R.string.label_no_data);
        } else {
            bindingObject.stateMessage.setText("");
        }
    }

    @Override
    public void showLoadContactsFailure() {
        showMessage(R.string.error_load_contacts);
        bindingObject.stateMessage.setText(R.string.label_no_data);
    }

    @Override
    public String getScreenTag() {
        return MainFragment.class.getSimpleName();
    }

    @Nullable
    @Override
    protected MainContract.Presenter providePresenter() {
        return presenter;
    }

    private void setupMenu() {
        bindingObject.toolbar.inflateMenu(R.menu.main);
        MenuItem searchItem = bindingObject.toolbar.getMenu().findItem(R.id.action_search);

        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                String adaptedQuery = query.toLowerCase();
                //Show all data if the query is empty
                if (adaptedQuery.replaceAll(" ", "").isEmpty()) {
                    contactsAdapter.swapData(allData);
                    recyclerViewExpandableItemManager.expandAll();
                    bindingObject.stateMessage.setText("");
                    return true;
                }
                List<Group> searchResult = new LinkedList<>();
                for (Group group : allData) {
                    Group resultGroup = new Group().setGroupName(group.getGroupName()).setPeople(new LinkedList<>());
                    for (Contact contact : group.getPeople()) {
                        if (contact.getFirstName().toLowerCase().contains(adaptedQuery) ||
                                contact.getLastName().toLowerCase().contains(adaptedQuery) ||
                                contact.getStatusIcon().contains(adaptedQuery) ||
                                contact.getStatusMessage().toLowerCase().contains(adaptedQuery)) {
                            resultGroup.getPeople().add(contact);
                        }
                    }
                    if (!resultGroup.getPeople().isEmpty()) {
                        searchResult.add(resultGroup);
                    }
                }
                contactsAdapter.swapData(searchResult);
                //Update state message
                if (searchResult.isEmpty()) {
                    bindingObject.stateMessage.setText(R.string.label_no_search_result);
                } else {
                    bindingObject.stateMessage.setText("");
                }
                return true;
            }
        });
    }
}