package com.dnod.cloudcontacts.ui.main;

import com.dnod.cloudcontacts.di.ActivityScoped;
import com.dnod.cloudcontacts.di.FragmentScoped;
import com.dnod.cloudcontacts.ui.Conductor;
import com.dnod.cloudcontacts.ui.base.BaseFragment;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainActivityModule {

    @Provides
    @ActivityScoped
    static Conductor<BaseFragment> provideConductor(MainActivity activity) {
        return activity;
    }

    @FragmentScoped
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainFragment addMainFragment();
}
