package com.dnod.cloudcontacts.ui.main;

import com.dnod.cloudcontacts.data.source.ContactsDataSource;
import com.dnod.cloudcontacts.ui.base.BaseScreenPresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

public final class MainPresenter extends BaseScreenPresenter<MainContract.View> implements MainContract.Presenter {

    private final ContactsDataSource dataSource;

    @Inject
    public MainPresenter(ContactsDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    protected void onViewDetached() {
    }

    @Override
    protected void onViewAttached() {
    }

    @Override
    protected void onScreenDestroyed() {
    }

    @Override
    public void loadContacts() {
        viewSubscriptions.add(dataSource.getContacts()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    getView().showContacts(result);
                }, throwable -> {
                    getView().showLoadContactsFailure();
                }));
    }
}
