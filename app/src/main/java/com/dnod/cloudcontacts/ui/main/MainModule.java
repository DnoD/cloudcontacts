package com.dnod.cloudcontacts.ui.main;

import com.dnod.cloudcontacts.di.FragmentScoped;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MainModule {

    @FragmentScoped
    @Binds
    abstract MainContract.Presenter mainPresenter(MainPresenter presenter);
}
