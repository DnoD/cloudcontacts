package com.dnod.cloudcontacts.ui;

public interface Conductor<T> {

    void goTo(T t);

    void goBack();
}
