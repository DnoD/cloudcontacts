package com.dnod.cloudcontacts.ui;

public interface BackInterceptor {

    boolean handleBackPress();
}
