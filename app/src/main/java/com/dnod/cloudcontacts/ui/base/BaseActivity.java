package com.dnod.cloudcontacts.ui.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.View;

import dagger.android.support.DaggerAppCompatActivity;

public abstract class BaseActivity extends DaggerAppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected Snackbar makeSnackBar(@StringRes int message, int duration) {
        return makeSnackBar(getString(message), duration);
    }

    protected Snackbar makeSnackBar(@NonNull String message, int duration) {
        return Snackbar.make(getRootView(), message, duration);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected abstract View getRootView();
}
