package com.dnod.cloudcontacts.ui.main;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.dnod.cloudcontacts.R;
import com.dnod.cloudcontacts.data.Contact;
import com.dnod.cloudcontacts.data.Gender;
import com.dnod.cloudcontacts.data.Group;
import com.dnod.cloudcontacts.data.GroupType;
import com.dnod.cloudcontacts.data.Status;
import com.dnod.cloudcontacts.databinding.ItemContactBinding;
import com.dnod.cloudcontacts.databinding.ItemGroupBinding;
import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;

import java.util.LinkedList;
import java.util.List;

public class ContactsAdapter extends AbstractExpandableItemAdapter<ContactsAdapter.GroupHolder, ContactsAdapter.ContactHolder> {

    private final List<Group> data = new LinkedList<>();
    private final LayoutInflater layoutInflater;

    public ContactsAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        setHasStableIds(true);
    }

    public void swapData(List<Group> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildCount(int groupPosition) {
        return data.get(groupPosition).getPeople().size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return data.get(groupPosition).getGroupName().hashCode() / 1000;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        Contact contact = data.get(groupPosition).getPeople().get(childPosition);
        return (contact.getFirstName() + contact.getLastName()).hashCode() / 1000;
    }

    @Override
    public GroupHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        return new GroupHolder(DataBindingUtil.inflate(layoutInflater, R.layout.item_group, parent, false));
    }

    @Override
    public ContactHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        return new ContactHolder(DataBindingUtil.inflate(layoutInflater, R.layout.item_contact, parent, false));
    }

    @Override
    public void onBindGroupViewHolder(GroupHolder holder, int groupPosition, int viewType) {
        final int expandState = holder.getExpandStateFlags();
        boolean isExpanded = (expandState & ExpandableItemConstants.STATE_FLAG_IS_EXPANDED) != 0;
        holder.binding.expandStateIcon.setImageResource((isExpanded) ? R.drawable.ic_arrow_drop_up : R.drawable.ic_arrow_drop_down);
        switch (GroupType.valueOf(data.get(groupPosition).getGroupName().toUpperCase())) {
            case WORK:
                holder.binding.groupName.setText(R.string.label_work);
                break;
            case FAMILY:
                holder.binding.groupName.setText(R.string.label_family);
                break;
            case SCHOOL:
                holder.binding.groupName.setText(R.string.label_school);
                break;
            case FRIENDS:
                holder.binding.groupName.setText(R.string.label_friends);
                break;
        }
    }

    @Override
    public void onBindChildViewHolder(ContactHolder holder, int groupPosition, int childPosition, int viewType) {
        Contact contact = data.get(groupPosition).getPeople().get(childPosition);
        if (contact.getGender() == null || contact.getGender().isEmpty()) {
            holder.binding.avatar.setImageResource(R.drawable.ic_avatar_unknown);
        } else {
            holder.binding.avatar.setImageResource((contact.getGender().equalsIgnoreCase(Gender.FEMALE.name()) ? R.drawable.ic_avatar_female : R.drawable.ic_avatar_male));
        }
        holder.binding.name.setText(layoutInflater.getContext().getString(R.string.item_contact_name_format, contact.getFirstName(), contact.getLastName()));
        switch (Status.valueOf(contact.getStatusIcon().toUpperCase())) {
            case AWAY:
                holder.binding.statusIcon.setImageResource(R.drawable.ic_status_away);
                break;
            case BUSY:
                holder.binding.statusIcon.setImageResource(R.drawable.ic_status_busy);
                break;
            case ONLINE:
                holder.binding.statusIcon.setImageResource(R.drawable.ic_status_online);
                break;
            case OFFLINE:
                holder.binding.statusIcon.setImageResource(R.drawable.ic_status_offline);
                break;
            case CALLFORWARDING:
                holder.binding.statusIcon.setImageResource(R.drawable.ic_status_pending);
                break;
        }
        holder.binding.statusMessage.setText(contact.getStatusMessage());
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(GroupHolder holder, int groupPosition, int x, int y, boolean expand) {
        return true;
    }

    final class GroupHolder extends AbstractExpandableItemViewHolder {

        ItemGroupBinding binding;

        GroupHolder(ItemGroupBinding groupBinding) {
            super(groupBinding.getRoot());
            binding = groupBinding;
        }
    }

    final class ContactHolder extends AbstractExpandableItemViewHolder {

        ItemContactBinding binding;

        ContactHolder(ItemContactBinding contactBinding) {
            super(contactBinding.getRoot());
            binding = contactBinding;
        }
    }
}
