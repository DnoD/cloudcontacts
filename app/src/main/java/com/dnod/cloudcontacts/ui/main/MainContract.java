package com.dnod.cloudcontacts.ui.main;

import com.dnod.cloudcontacts.ScreeView;
import com.dnod.cloudcontacts.ScreenPresenter;
import com.dnod.cloudcontacts.data.Group;

import java.util.List;

public interface MainContract {
    interface View extends ScreeView<Presenter> {

        void showContacts(List<Group> data);

        void showLoadContactsFailure();
    }

    interface Presenter extends ScreenPresenter<View> {

        void loadContacts();
    }
}
