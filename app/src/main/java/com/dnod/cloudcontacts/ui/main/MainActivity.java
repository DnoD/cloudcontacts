package com.dnod.cloudcontacts.ui.main;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.dnod.cloudcontacts.R;
import com.dnod.cloudcontacts.databinding.ActivityMainBinding;
import com.dnod.cloudcontacts.ui.Conductor;
import com.dnod.cloudcontacts.ui.base.BaseActivity;
import com.dnod.cloudcontacts.ui.base.BaseFragment;

public class MainActivity extends BaseActivity implements Conductor<BaseFragment> {

    private ActivityMainBinding bindingObject;
    private BaseFragment currFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindingObject = DataBindingUtil.setContentView(this, R.layout.activity_main);
        goTo(MainFragment.createInstance());
    }

    @Override
    public void goTo(BaseFragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, currFragment = fragment, fragment.getScreenTag())
                .commit();
    }

    @Override
    public void goBack() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (currFragment != null && currFragment.handleBackPress()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected View getRootView() {
        return bindingObject.getRoot();
    }
}
