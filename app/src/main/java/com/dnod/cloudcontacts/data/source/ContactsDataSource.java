package com.dnod.cloudcontacts.data.source;

import com.dnod.cloudcontacts.data.Group;

import java.util.List;

import io.reactivex.Observable;

public interface ContactsDataSource {

    Observable<List<Group>> getContacts();
}
