package com.dnod.cloudcontacts.data;

public enum Status {
    ONLINE, AWAY, OFFLINE, BUSY, CALLFORWARDING
}
