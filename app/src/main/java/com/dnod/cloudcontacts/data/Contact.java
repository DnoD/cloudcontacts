package com.dnod.cloudcontacts.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Contact implements Parcelable {

    private String firstName;
    private String lastName;
    private String statusIcon;
    private String gender;

    public Contact() {}

    public String getGender() {
        return gender;
    }

    public Contact setGender(String gender) {
        this.gender = gender;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Contact setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Contact setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getStatusIcon() {
        return statusIcon;
    }

    public Contact setStatusIcon(String statusIcon) {
        this.statusIcon = statusIcon;
        return this;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public Contact setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    private String statusMessage;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(statusIcon);
        dest.writeString(statusMessage);
        dest.writeString(gender);
    }

    protected Contact(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
        statusIcon = in.readString();
        statusMessage = in.readString();
        gender = in.readString();
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };
}
