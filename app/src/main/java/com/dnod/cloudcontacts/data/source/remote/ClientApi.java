package com.dnod.cloudcontacts.data.source.remote;

import com.dnod.cloudcontacts.data.source.remote.response.ContactsResponse;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;

public interface ClientApi {

    @GET("download?path=%2F&files=contacts.json")
    Observable<ContactsResponse> getContacts();
}
