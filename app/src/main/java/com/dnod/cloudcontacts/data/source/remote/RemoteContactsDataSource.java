package com.dnod.cloudcontacts.data.source.remote;

import com.dnod.cloudcontacts.data.Group;
import com.dnod.cloudcontacts.data.source.ContactsDataSource;
import com.dnod.cloudcontacts.di.Endpoint;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RemoteContactsDataSource implements ContactsDataSource {

    private final static long CONNECT_TIMEOUT = TimeUnit.SECONDS.toMillis(3);
    private final static int MAX_RETRY = 3;
    private final ClientApi clientApi;

    @Inject
    public RemoteContactsDataSource(@Endpoint String endpoint, Gson gson) {
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(@NonNull Chain chain) throws IOException {
                        Request request = chain.request();
                        return proceed(chain, request, 0);
                    }

                    private Response proceed(Chain chain, Request request, int retryCount) throws IOException {
                        Response response = null;
                        try {
                            response = chain.proceed(request);
                            if (!response.isSuccessful() && retryCount < MAX_RETRY) {
                                return proceed(chain, request, ++retryCount);
                            }
                        } catch (IOException e) {
                            if (retryCount < MAX_RETRY) {
                                return proceed(chain, request, ++retryCount);
                            } else {
                                throw e;
                            }
                        } catch (OutOfMemoryError error) {
                            throw new IOException();
                        }
                        return response;
                    }
                })
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        Retrofit retrofitClient = new Retrofit.Builder()
                .baseUrl(endpoint)
                .client(okHttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        clientApi = retrofitClient.create(ClientApi.class);
    }

    @Override
    public Observable<List<Group>> getContacts() {
        return clientApi.getContacts()
                .subscribeOn(Schedulers.io())
                .map(response -> response.groups);
    }
}
