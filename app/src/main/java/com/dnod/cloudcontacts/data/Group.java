package com.dnod.cloudcontacts.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Group implements Parcelable {

    private String groupName;
    private List<Contact> people;

    public Group() {}

    public String getGroupName() {
        return groupName;
    }

    public Group setGroupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public List<Contact> getPeople() {
        return people;
    }

    public Group setPeople(List<Contact> people) {
        this.people = people;
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(groupName);
        dest.writeTypedList(people);
    }

    protected Group(Parcel in) {
        groupName = in.readString();
        people = in.createTypedArrayList(Contact.CREATOR);
    }

    public static final Creator<Group> CREATOR = new Creator<Group>() {
        @Override
        public Group createFromParcel(Parcel in) {
            return new Group(in);
        }

        @Override
        public Group[] newArray(int size) {
            return new Group[size];
        }
    };
}
