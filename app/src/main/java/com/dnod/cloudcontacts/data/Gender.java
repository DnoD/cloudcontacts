package com.dnod.cloudcontacts.data;

public enum Gender {
    FEMALE, MALE
}
