package com.dnod.cloudcontacts.data.source.remote.response;

import com.dnod.cloudcontacts.data.Group;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContactsResponse {

    @SerializedName("groups")
    @Expose
    public List<Group> groups;
}
