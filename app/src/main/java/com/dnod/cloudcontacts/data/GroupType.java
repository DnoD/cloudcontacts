package com.dnod.cloudcontacts.data;

public enum GroupType {
    FAMILY, WORK, SCHOOL, FRIENDS
}
