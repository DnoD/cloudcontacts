package com.dnod.cloudcontacts;

import android.content.Context;

import com.dnod.cloudcontacts.di.ApplicationComponent;
import com.dnod.cloudcontacts.di.DaggerApplicationComponent;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class CloudContactsApplication extends DaggerApplication {
    private static CloudContactsApplication sInstance;

    public static Context getInstance() {
        return sInstance;
    }

    private ApplicationComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        mAppComponent = DaggerApplicationComponent.builder().application(this).build();
        mAppComponent.inject(this);
        return mAppComponent;
    }

    public static ApplicationComponent getApplicationInjector() {
        return sInstance.mAppComponent;
    }
}