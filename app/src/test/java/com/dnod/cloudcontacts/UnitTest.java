package com.dnod.cloudcontacts;

import com.dnod.cloudcontacts.data.Contact;
import com.dnod.cloudcontacts.data.Gender;
import com.dnod.cloudcontacts.data.Group;
import com.dnod.cloudcontacts.data.Status;
import com.dnod.cloudcontacts.data.source.ContactsDataSource;
import com.dnod.cloudcontacts.data.source.remote.RemoteContactsDataSource;
import com.dnod.cloudcontacts.ui.main.MainContract;
import com.dnod.cloudcontacts.ui.main.MainPresenter;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class UnitTest {


    protected static ContactsDataSource mockDataSource;

    @BeforeClass
    public static void setup() {
        mockDataSource = mock(RemoteContactsDataSource.class);
    }

    @BeforeClass
    public static void setUpRxSchedulers() {
        Scheduler immediate = new Scheduler() {
            @Override
            public Disposable scheduleDirect(@NonNull Runnable run, long delay, @NonNull TimeUnit unit) {
                // this prevents StackOverflowErrors when scheduling with a delay
                return super.scheduleDirect(run, 0, unit);
            }

            @Override
            public Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(Runnable::run);
            }
        };

        RxJavaPlugins.setInitIoSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitComputationSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitNewThreadSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitSingleSchedulerHandler(scheduler -> immediate);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> immediate);
    }

    @Test
    public void testLoadData() {
        MainContract.Presenter presenter = new MainPresenter(mockDataSource);
        MainContract.View view = mock(MainContract.View.class);
        presenter.takeView(view);

        List<Group> contacts = generateMockContacts();
        ArgumentCaptor<List<Group>> contactsCaptor = ArgumentCaptor.forClass(List.class);
        Observable<List<Group>> result = Observable.just(contacts);
        when(mockDataSource.getContacts()).thenReturn(result);

        presenter.loadContacts();

        verify(view, times(1)).showContacts(contactsCaptor.capture());

        assertEquals(contactsCaptor.getValue().size(), contacts.size());
        assertEquals(contactsCaptor.getValue().get(0).getGroupName(), contacts.get(0).getGroupName());
        assertEquals(contactsCaptor.getValue().get(0).getPeople().size(), contacts.get(0).getPeople().size());

        result.test().assertSubscribed()
                .assertNoErrors()
                .assertValue(contacts)
                .assertComplete();
    }

    @Test
    public void testLoadDataFailure() {

        MainContract.Presenter presenter = new MainPresenter(mockDataSource);
        MainContract.View view = mock(MainContract.View.class);
        presenter.takeView(view);


        final Exception error = new Exception("Nullable");
        Observable<List<Group>> result = Observable.error(error);
        when(mockDataSource.getContacts()).thenReturn(result);

        presenter.loadContacts();

        verify(view, times(1)).showLoadContactsFailure();

        result.test().assertSubscribed()
                .assertError(error);
    }

    private List<Group> generateMockContacts() {
        List<Group> contacts = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            List<Contact> people = new ArrayList<>();
            people.add(new Contact()
                    .setFirstName("One")
                    .setLastName("OneLast")
                    .setGender(Gender.values()[new Random().nextInt(Gender.values().length - 1)].name())
                    .setStatusIcon(Status.values()[new Random().nextInt(Status.values().length - 1)].name()));
            people.add(new Contact()
                    .setFirstName("Two")
                    .setLastName("TwoLast")
                    .setGender(Gender.values()[new Random().nextInt(Gender.values().length - 1)].name())
                    .setStatusIcon(Status.values()[new Random().nextInt(Status.values().length - 1)].name()));
            contacts.add(new Group().setGroupName("GroupName" + i)
                    .setPeople(people));
        }
        return contacts;
    }
}